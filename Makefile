server_bin = chainmaker-archive-service
version = v1.0.0
CURRENT_PATH = $(shell pwd)
PROJECT_PATH = $(shell dirname $(CURRENT_PATH))
MOUNT_PATH = ${PROJECT_PATH}/archive-service-mount

.PHONY: build 
build:
	mkdir -p bin; \
	cd src; \
	export GODEBUG=x509ignoreCN=0; \
	go build -o ../bin/$(server_bin); \
	cd ..

.PHONY: bfdb-clean-tools
bfdb-clean-tools:
	cd bfdb-clean-tools; \
	go build 

.PHONY: start
start:
	cd bin ; \
	nohup ./$(server_bin) >output 2>&1 &

.PHONY: stop
stop:
	./stop_server.sh $(server_bin)

.PHONY: docker-build
docker-build: build 
	rm -rf log/ service_datas/ ;\
	docker build -t $(server_bin) -f ./Dockerfile . ;\
	docker tag $(server_bin) $(server_bin):$(version) ;


.PHONY: docker-start 
docker-start:	
	if [ ! -d "${MOUNT_PATH}" ]; then \
		mkdir -p ${MOUNT_PATH}/service_datas ;\
		mkdir -p ${MOUNT_PATH}/log ;\
		cp -rf ${CURRENT_PATH}/configs ${MOUNT_PATH}/ ; \
	fi
	./start_docker_server.sh $(MOUNT_PATH)


.PHONY: docker-stop 
docker-stop:
	./stop_docker_server.sh $(server_bin)

.PHONY: clean
clean:
	rm -rf bin 

ut:
	./ut_cover.sh

lint:
	golangci-lint run ./...