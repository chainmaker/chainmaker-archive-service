//go:build darwin
// +build darwin

package filestore

import (
	"fmt"
	"os"
	"syscall"
)

// GetLastModify get last modify time
// @Description:
// @param file
// @return int64
// @return error
func GetLastModify(file os.FileInfo) (int64, error) {
	sys, ok := file.Sys().(*syscall.Stat_t)
	if !ok {
		return 0, fmt.Errorf("get file stat error")
	}
	return sys.Atimespec.Sec, nil
}
