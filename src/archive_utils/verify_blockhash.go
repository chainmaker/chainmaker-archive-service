/*
Copyright (C) BABEC. All rights reserved.


SPDX-License-Identifier: Apache-2.0
*/

// Package archive_utils define common utils
package archive_utils

import (
	"bytes"
	"fmt"
	"sync"

	"chainmaker.org/chainmaker/common/v2/crypto/hash"
	"chainmaker.org/chainmaker/pb-go/v2/common"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	"chainmaker.org/chainmaker/utils/v2"
)

const (
	verifySuccess = "success"
)

// VerifyBlockHash 验证区块hash头
// @param hashType
// @pram block
// @return bool
// @return error
func VerifyBlockHash(hashType string, block *common.BlockInfo, skipVerifyPayload bool) (bool, string, error) {
	calculateBlockHash, hashError := utils.CalcBlockHash(hashType, block.Block)
	if hashError != nil {
		return false, hashError.Error(), hashError
	}
	if !bytes.Equal(calculateBlockHash, block.Block.Header.BlockHash) {
		return false, "block header hash not equal", nil
	}
	if skipVerifyPayload {
		return true, verifySuccess, nil
	}
	// 校验区块payload
	passed, detail, payloadErr := verifyBlockPayload(hashType, block)
	if payloadErr != nil {
		return false, payloadErr.Error(), payloadErr
	}
	if !passed {
		return false, detail, nil
	}
	return true, verifySuccess, nil
}

// verfity block tx,rwset,dag hash
func verifyBlockPayload(hashType string, block *common.BlockInfo) (bool, string, error) {
	txRWSetMap := make(map[string]*common.TxRWSet)
	for i := 0; i < len(block.RwsetList); i++ {
		txRWSetMap[block.RwsetList[i].TxId] = block.RwsetList[i]
	}
	txCount := len(block.Block.Txs)
	errsC := make(chan error, txCount+3)
	txHashes := make([][]byte, txCount)
	wg := &sync.WaitGroup{}
	wg.Add(txCount)
	for i, tx := range block.Block.Txs {
		rwSet := txRWSetMap[tx.Payload.TxId]
		if rwSet == nil {
			rwSet = &commonPb.TxRWSet{
				TxId:     tx.Payload.TxId,
				TxReads:  nil,
				TxWrites: nil,
			}
		}
		go func(tx *commonPb.Transaction, rwSet *commonPb.TxRWSet, x int) {
			defer wg.Done()
			var err error
			txHashes[x], err = getTxHash(tx, rwSet, hashType, block.Block.Header)
			if err != nil {
				errsC <- err
			}

		}(tx, rwSet, i)
	}
	wg.Wait()
	if len(errsC) > 0 {
		err := <-errsC
		return false, err.Error(), err
	}
	wg.Add(3)
	//calc tx root
	var txRoot []byte
	go func() {
		defer wg.Done()
		var err error
		txRoot, err = hash.GetMerkleRoot(hashType, txHashes)
		if err != nil {

			errsC <- fmt.Errorf("get tx merkle root error %s", err)
		}
	}()
	//calc rwset root
	var rwsetRoot []byte
	go func() {
		defer wg.Done()
		var err error
		rwsetRoot, err = utils.CalcRWSetRoot(hashType, block.Block.Txs)
		if err != nil {

			errsC <- fmt.Errorf("get rwset merkle root error %s", err)
		}
	}()
	//calc dag hash
	var dagHash []byte
	go func() {
		defer wg.Done()
		// DagDigest

		var err error
		dagHash, err = utils.CalcDagHash(hashType, block.Block.Dag)
		if err != nil {

			errsC <- fmt.Errorf("get dag hash error %s", err)
		}
	}()
	wg.Wait()
	// not close errsC will NOT cause memory leak
	if len(errsC) > 0 {
		err := <-errsC
		return false, err.Error(), err
	}
	if !bytes.Equal(txRoot, block.Block.Header.TxRoot) {
		return false, "txRoot not match", nil
	}
	if !bytes.Equal(rwsetRoot, block.Block.Header.RwSetRoot) {
		return false, "rwset not match", nil
	}
	if !bytes.Equal(dagHash, block.Block.Header.DagHash) {
		return false, "daghash not match", nil
	}
	return true, verifySuccess, nil
}

// IsDagHashValid to check if block dag equals with simulated block dag
// @param block
// @param hashType
// @return error
func IsDagHashValid(block *commonPb.Block, hashType string) error {
	dagHash, err := utils.CalcDagHash(hashType, block.Dag)
	if err != nil || !bytes.Equal(dagHash, block.Header.DagHash) {
		return fmt.Errorf("dag expect %x, got %x", block.Header.DagHash, dagHash)
	}
	return nil
}

// IsRWSetHashValid to check if read write set is valid
// @param block
// @param hashType
// @param error
func IsRWSetHashValid(block *commonPb.Block, hashType string) error {
	rwSetRoot, err := utils.CalcRWSetRoot(hashType, block.Txs)
	if err != nil {
		return fmt.Errorf("calc rwset error, %s", err)
	}
	if !bytes.Equal(rwSetRoot, block.Header.RwSetRoot) {
		return fmt.Errorf("rwset expect %x, got %x", block.Header.RwSetRoot, rwSetRoot)
	}
	return nil
}

// // IsMerkleRootValid to check if block merkle root equals with simulated merkle root
// // @param block
// // @pram txHashes
// // @param hashType
// // @return error
// func IsMerkleRootValid(block *commonPb.Block, txHashes [][]byte, hashType string) error {
// 	txRoot, err := hash.GetMerkleRoot(hashType, txHashes)
// 	if err != nil || !bytes.Equal(txRoot, block.Header.TxRoot) {
// 		return fmt.Errorf("GetMerkleRoot(%s,%v) get %x ,txroot expect %x, got %x, err: %s",
// 			hashType, txHashes, txRoot, block.Header.TxRoot, txRoot, err)
// 	}
// 	return nil
// }

func getTxHash(tx *commonPb.Transaction,
	rwSet *commonPb.TxRWSet,
	hashType string,
	blockHeader *commonPb.BlockHeader) (
	[]byte, error) {
	var rwSetHash []byte
	rwSetHash, err := utils.CalcRWSetHash(hashType, rwSet)
	if err != nil {
		return nil, err
	}
	if tx.Result == nil {
		// in case tx.Result is nil, avoid panic
		e := fmt.Errorf("tx(%s) result == nil", tx.Payload.TxId)
		return nil, e
	}
	tx.Result.RwSetHash = rwSetHash
	// calculate complete tx hash, include tx.Header, tx.Payload, tx.Result
	var txHash []byte
	txHash, err = utils.CalcTxHashWithVersion(
		hashType, tx, int(blockHeader.BlockVersion))
	if err != nil {
		return nil, err
	}
	return txHash, nil
}
