module chainmaker.org/chainmaker-archive-service

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.2
	chainmaker.org/chainmaker/lws v1.1.0
	chainmaker.org/chainmaker/pb-go/v2 v2.3.3
	chainmaker.org/chainmaker/protocol/v2 v2.3.3
	chainmaker.org/chainmaker/store-leveldb/v2 v2.3.1
	chainmaker.org/chainmaker/utils/v2 v2.3.3
	github.com/aws/aws-sdk-go v1.35.3 // indirect
	github.com/coreos/etcd v3.3.25+incompatible // indirect
	github.com/dgraph-io/ristretto v0.1.0 // indirect
	github.com/gin-contrib/gzip v0.0.6
	github.com/gin-gonic/gin v1.8.1
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gogo/protobuf v1.3.2
	github.com/google/uuid v1.3.0 // indirect
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/mitchellh/mapstructure v1.4.2
	github.com/pingcap/errcode v0.3.0 // indirect
	github.com/pingcap/errors v0.11.5-0.20201126102027-b0a155152ca3 // indirect
	github.com/pingcap/failpoint v0.0.0-20200702092429-9f69995143ce // indirect
	github.com/pingcap/kvproto v0.0.0-20210219095907-b2375dcc80ad // indirect
	github.com/pingcap/log v0.0.0-20201112100606-8f1e84a3abc8 // indirect
	github.com/pingcap/sysutil v0.0.0-20201130064824-f0c8aa6a6966 // indirect
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.11.1
	github.com/spf13/viper v1.9.0
	github.com/stretchr/testify v1.8.0
	github.com/syndtr/goleveldb v1.0.1-0.20200815110645-5c35d600f0ca
	github.com/tidwall/tinylru v1.1.0
	go.etcd.io/etcd v3.3.25+incompatible // indirect
	go.uber.org/zap v1.17.0
	golang.org/x/text v0.3.8 // indirect
	golang.org/x/time v0.0.0-20220722155302-e5dcc9cfc0b9
	google.golang.org/grpc v1.40.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
