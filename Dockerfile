
FROM ubuntu:22.04
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN apt-get update && apt-get install -y vim net-tools tree p7zip-full p7zip-rar
ENV TZ "Asia/Shanghai"
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata && \
    echo $TZ > /etc/timezone && \
    ln -fs /usr/share/zoneinfo/$TZ /etc/localtime && \
    dpkg-reconfigure tzdata -f noninteractive  



WORKDIR  /chainmaker-archive-service
COPY . .

EXPOSE 13119 13120 13122 13123

WORKDIR /chainmaker-archive-service/bin
ENTRYPOINT [ "./chainmaker-archive-service" ,"-i"]
CMD [ "../configs/config.yml" ]