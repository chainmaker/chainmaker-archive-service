PATHMOUNT=$1
 
DOCKER_CONTAINER=$(docker container ls -a | grep chainmaker-archive-service | awk '{print $1}')
echo $DOCKER_CONTAINER

if [ ${#DOCKER_CONTAINER} -gt 1 ]; then 
    docker restart  $DOCKER_CONTAINER
else 
    docker run -p 13119:13119 -p 13120:13120 \
    --name chainmaker-archive-service \
    -v $PATHMOUNT/configs:/chainmaker-archive-service/configs \
    -v $PATHMOUNT/service_datas:/chainmaker-archive-service/service_datas  \
    -v $PATHMOUNT/log:/chainmaker-archive-service/log \
    -d chainmaker-archive-service:v1.0.0
fi 